from flask import Flask

app = Flask(__name__)

# simple GET request
@app.route("/")
def hello():
    return "Welcome to the Absolute WEB API Management System"


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port=9000)