# Absolute Web API Deployment Project


This repository contains different open-source frameworks to build, test and deploy containerized Absolute Web API platform.


## Setup


**AWS has been considered to deploy this infrastructure**



1. 3 virtual machine will require to provision this environment.(Ideal configuration would be 2v CPU and 4 GB Memory). One machine will host Jenkins instance , second machine will host SonarQube instance and third machine will host minikube instance.



2. I have clonned open source ansible roles inside `automation_script` directory. Assuming the hosts file has correct set of IPs. We can trigger main ansible playbook from our machine.



3. Based on the host file value, Ansible will install necessary utilities on the respective machines.


## Pipeline overview


1. I have made assumption that the `Jenkinsfile` will be placed inside the application repository.


2. We can trigger Jenkins job manually from the UI (Automatic webhook is possible but not implemented).


3. Jenkins will clone the code.


4. Jenkins will run code quality scan with the help of Sonar Qube server.


5. If code quality is fine then It will continue the job otherwise It will terminate the pipeline.


6. Jenkins will build the image locally and push it to the artifactory.


7. We pull the image from the artifactory and test it.


8. If the `curl` return 200 status code that means application is working at least, otherwise there is some problem with the application and pipeline marked as a fail.


*Sample Pipeline Diagram has been attached.*



## Improvement


**Due to time constrains I couldn't able to write helm chart.**


*We can write application helm chart and push it to the artifactory and then test it using Minikube.*


**For small experiment we can work with minikube, we don't need to setup heavy kubernetes cluster.**